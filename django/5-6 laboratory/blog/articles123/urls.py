from django.urls import path
from django.conf.urls import url

from . import views
from .views import get_article

urlpatterns = [
    path('', views.archive),
    path('<int:article_id>/',
        get_article,
        name='get_article'
    ),
    path('new/', views.create_post, name='new'),
    url(r'^register/$', views.RegisterFormView.as_view(), name='register'),
    url(r'^login/$', views.LoginFormView.as_view(), name='login'),
    url(r'^logout/$', views.logout_, name='logout'),
]