function rgb_to_hex(color) {
    var rgb = color.replace(/\s/g, '').match(/^rgba?\((\d+),(\d+),(\d+)/i);
    return (rgb && rgb.length === 4) ? "#" + ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) + ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) + ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : color;
}

$('td').bind('contextmenu', function(e) {
    return false;
});

$("td").mousedown(function() {
    switch (event.which) {
        case 1:
            $("body").css("color", rgb_to_hex($(this).css('color')));
            $('.color-table-input').val(rgb_to_hex($(this).css('color')));
            break;
        case 3:
            $("body").css("background", rgb_to_hex($(this).css('color')));
            $('.color-table-input').val(rgb_to_hex($(this).css('color')));
            break;
        default:
            $('.color-table-input').val(rgb_to_hex($(this).css('color')));
    }
});

$(".rgb-input").on('change', function() {
    var r = $(".rgb-input1").val();
    var g = $(".rgb-input2").val();
    var b = $(".rgb-input3").val();
    $(".window").css("background", `rgb(${r}, ${g}, ${b})`);
});